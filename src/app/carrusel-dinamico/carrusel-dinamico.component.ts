import { Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  /**
 * proporciona una manera fácil de crear un carrusel con contenido dinámico en tu aplicación Angular. Puedes utilizarlo dentro de tu aplicación para presentar contenido que se puede desplazar horizontalmente.
 * @example <app-carrusel-dinamico><!-- Contenido del carrusel aquí --></app-carrusel-dinamico>
 */
  selector: 'app-carrusel-dinamico',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './carrusel-dinamico.component.html',
  styleUrls: ['./carrusel-dinamico.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class CarruselDinamicoComponent {

  /**
   * Determines whether navigation buttons should appear.
   */
  buttonsAppear:boolean = false;
  /**
   * Stores the total width of the elements contained in the carousel.
   * This value is calculated as the product of the child size and the number of container objects.
   */
  objectsContained!:number;


  @ViewChild('contentContainer', { static: true }) contentContainer!: ElementRef;

   /**
   * Lifecycle hook that runs after the component's content has been initialized.
   * Calculates the total width of the contained elements and determines whether
   * navigation buttons should appear based on the parent size and content width.
   */
  ngAfterContentInit(): void {
    const parentSize = this.contentContainer.nativeElement.offsetParent.clientWidth;
    const childSize = this.contentContainer.nativeElement.firstChild.offsetWidth;
    const containerObjects = this.contentContainer.nativeElement.childElementCount;
    this.objectsContained = childSize * containerObjects;

    if (parentSize > this.objectsContained) {
      this.buttonsAppear = false;
    } else {
      this.buttonsAppear = true;
    }
  }

  /**
   * Handles navigation within the carousel.
   * @param rightLeft - Indicates whether to navigate left (true) or right (false).
   */
  navigation(rightLeft:boolean):void{
    const offsetWidth = this.contentContainer.nativeElement.firstChild.offsetWidth;
    let translation = this.contentContainer.nativeElement.scrollLeft;

    console.log(translation + ', ' + this.objectsContained);

    if(this.objectsContained == translation ){
      
    }

    if (rightLeft) {
      translation -= offsetWidth;
    } else {
      translation += offsetWidth;
    }

    this.contentContainer.nativeElement.scroll({ left: translation, behavior: 'smooth' });
  }

}
