import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarruselDinamicoComponent } from './carrusel-dinamico.component';

describe('CarruselDinamicoComponent', () => {
  let component: CarruselDinamicoComponent;
  let fixture: ComponentFixture<CarruselDinamicoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CarruselDinamicoComponent]
    });
    fixture = TestBed.createComponent(CarruselDinamicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
