import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CarruselDinamicoComponent } from './carrusel-dinamico/carrusel-dinamico.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CarruselDinamicoComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
